package test;

import com.ibm.common.CommonActions;
import com.ibm.utils.Utils;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

public class submit_data {
    WebDriver webdriver;

    String currentDir;

    @Test
    public void submit() throws InterruptedException, IOException {
        String inputString="";
        String app_url = "https://www.google.com/";
        String process_name="chromedriver.exe (32 bit)";
        String[] Value=new String[3];
        Utils utl=new Utils();
        int i,j, rowNum;

        CommonActions cm=new CommonActions();

        //Close any running chrome instances
        Runtime rt= Runtime.getRuntime();
        try{
            Process pr =rt.exec("taskKill /F /IM" + process_name );
        }catch (IOException e){
            e.printStackTrace();

        }
        currentDir=System.getProperty("user.dir");

        System.setProperty("webdriver.chrome.driver",currentDir+"\\src\\test\\resources\\chromedriver.exe");
        webdriver=new ChromeDriver();
        webdriver.manage().window().maximize();

        //Launch application
        webdriver.get(app_url);

        WebDriverWait wait=new WebDriverWait(webdriver, 10);
//        Read file content

        FileInputStream file = new FileInputStream(new File("C:\\Users\\Anil\\IdeaProjects\\Create_MQ_Data\\src\\test\\resources\\data\\TestData.xlsx"));

        //Create Workbook instance holding reference to .xlsx file
        XSSFWorkbook workbook = new XSSFWorkbook(file);

        //Get first/desired sheet from the workbook
        XSSFSheet sheet = workbook.getSheetAt(0);

        //Iterate through each rows one by one
        rowNum=sheet.getLastRowNum();
        Row row;

       for (j=1;j<rowNum;j++){
           System.out.println("Preparing Data for iteration: " + j);
             row = sheet.getRow(j);
            int noofColm = row.getLastCellNum();
            for (i=0; i < noofColm; i++){
                Value[i]=row.getCell(i).getStringCellValue();
                System.out.println(Value[i]);
            }
            inputString=utl.replace_data(Value);
        }

       WebElement inputBox = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("input[name='q']")));
        if (inputString != "" ) {
            inputBox.sendKeys(inputString);
            WebElement searchButton = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("input[name='btnK']")));
            searchButton.click();
            //enter and submit
            Thread.sleep(30);
        }
        else{
            System.out.println("Input Data is blank");
        }

        webdriver.quit();
    }
}
