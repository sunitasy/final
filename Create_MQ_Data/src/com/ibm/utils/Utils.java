package com.ibm.utils;

import java.io.*;

public class Utils {
    public String replace_data(String[] val) {
        String oldContent = "";
        BufferedReader reader = null;
        FileWriter writer = null;
        String newContent="";

        try {
            reader = new BufferedReader(new FileReader("C:\\Users\\Anil\\IdeaProjects\\Create_MQ_Data\\src\\test\\resources\\data\\template"));
            //Reading all the lines of input text file into oldContent
            String line = reader.readLine();
            while (line != null) {
                oldContent = oldContent + line + System.lineSeparator();
                line = reader.readLine();
            }

            //Replacing oldString with newString in the oldContent
            newContent = oldContent.replaceAll("PARAMETER1", val[0]);
            newContent = newContent.replaceAll("PARAMETER2", val[1]);
            newContent = newContent.replaceAll("PARAMETER3", val[2]);

            System.out.println("***************************************************");
            System.out.println(newContent);

        } catch (IOException e) {
            e.printStackTrace();
        }
        return newContent;
    }
}
