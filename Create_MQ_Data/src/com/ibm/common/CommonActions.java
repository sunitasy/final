package com.ibm.common;

import com.ibm.utils.Utils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Iterator;

public class CommonActions {
    public String readExcel() throws IOException {
        String[] Value=new String[3];
        Utils utl=new Utils();
        int noofColm, i;
        String str="";

        try {
            FileInputStream file = new FileInputStream(new File("C:\\Users\\Anil\\IdeaProjects\\Create_MQ_Data\\src\\test\\resources\\data\\TestData.xlsx"));

            //Create Workbook instance holding reference to .xlsx file
            XSSFWorkbook workbook = new XSSFWorkbook(file);

            //Get first/desired sheet from the workbook
            XSSFSheet sheet = workbook.getSheetAt(0);

            //Iterate through each rows one by one
            Iterator<Row> rowIterator = sheet.iterator();
            while (rowIterator.hasNext()) {
                Row row = rowIterator.next();
                //For each row, iterate through all the columns
                 noofColm=row.getLastCellNum();
                for (i=0; i < noofColm; i++){
                    Value[i]=row.getCell(i).getStringCellValue();
                    System.out.println(Value[i]);
                }
                str=utl.replace_data(Value);

            }
            file.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return str;
    }
}
